package application;

import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class DoctorView {
	
	public Tab init() throws ClassNotFoundException, SQLException{
		Doctor doctor = new Doctor();
		Tab tab1 = new Tab();
		tab1.setText("Doctor view and create TimeSheet");
		VBox vbox = new VBox(8); 
		Pane tab1Pane = new Pane();
		tab1Pane.setMinWidth(600.0);
		final DatePicker datePicker = new DatePicker();
		Render.setPosition(datePicker, 180.0, 20.0);
		
		// comboBox contains doctor name selection
		final ComboBox<String> doctorComboList = new ComboBox<String>();	
		ArrayList<Doctor> doctorList = doctor.getDoctor();
		doctorList.forEach(d->{doctorComboList.getItems().add(d.getFirstName());});
		
		ArrayList<ComboBox> timeListCombo = createComboBoxList();	
		
		Button viewTimeSheet = new Button("View My TimeSheet");
		final Button submitTimeSheet = new Button("Submit");
		Render.setPosition(submitTimeSheet, 600, 100);
		
		final Label label = new Label("TimeSheet View");
		final Label notice = new Label();
		
		TableView<TimeSheet> table = new TableView<TimeSheet>();
		table.setMinSize(500.0, 500.0);
		TableColumn<TimeSheet, Long> timeId = new TableColumn<TimeSheet, Long>("TimeId");
		timeId.setMinWidth(100.0);
		TableColumn<TimeSheet, Date> date = new TableColumn<TimeSheet, Date>("Date");
		date.setMinWidth(100.0);
		TableColumn<TimeSheet, Time> start_time = new TableColumn<TimeSheet, Time>("Start Time ");
		start_time.setMinWidth(100.0);
		TableColumn<TimeSheet, Time> end_time = new TableColumn<TimeSheet, Time>("End Time");
		end_time.setMinWidth(100.0);
		TableColumn<TimeSheet, String> booking_status = new TableColumn<TimeSheet, String>("Booking");
		booking_status.setMinWidth(100.0);
		table.getColumns().setAll(timeId, date, start_time,end_time,booking_status);
       
		tab1Pane.getChildren().addAll(doctorComboList,viewTimeSheet,table,datePicker,submitTimeSheet,label,notice);
		
		timeListCombo.forEach(c->{tab1Pane.getChildren().add(c);});
	
        tab1.setContent(tab1Pane);

		Node[] nodeList = {doctorComboList,viewTimeSheet,label,table};
		setGroupPosition(nodeList);
		
		viewTimeSheet.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		    	String doctorName = doctorComboList.getValue();
		    	LocalDate dateTime = datePicker.getValue();
		    	ArrayList<TimeSheet> timeSheetList = null;
		    	ObservableList<TimeSheet> data = null;
		    	try {
					timeSheetList =doctor.getTimeSheet(dateTime, doctorName);
					data = FXCollections.observableArrayList(timeSheetList);
				} catch (ClassNotFoundException | SQLException e1) {
					e1.printStackTrace();
				}
		    			    	
				timeId.setCellValueFactory(new PropertyValueFactory<TimeSheet, Long>("idTimeSheet"));
				date.setCellValueFactory(new PropertyValueFactory<TimeSheet, Date>("date"));	
		        start_time.setCellValueFactory(new PropertyValueFactory<TimeSheet, Time>("start_time"));
		        end_time.setCellValueFactory(new PropertyValueFactory<TimeSheet, Time>("end_time"));
		        booking_status.setCellValueFactory(new PropertyValueFactory<TimeSheet, String>("booking_status"));
		        table.setItems(data);
		    }
		});
		
		submitTimeSheet.setOnAction(event->{ 
			LocalTime period_start = null;
			LocalTime period_end = null;
			String doctorName = doctorComboList.getValue();
			int doctorId = 0;
			for(Doctor doc: doctorList){
				if(doc.getFirstName() == doctorName){
					doctorId = doc.getDoctorId();
				}
			}
			LocalDate dateTime = datePicker.getValue();
			
			if(dateTime.isBefore(LocalDate.now())){
				notice.setText("Can not add");
				System.out.println("false");
				return;
			}
			
			for(int i = 0; i< timeListCombo.size(); i=i+2){		
				
				if(timeListCombo.get(i).getValue() == null && timeListCombo.get(i+1).getValue() == null){
					continue;
				}
				
				if(timeListCombo.get(i).getValue() == null || timeListCombo.get(i+1).getValue() == null){
					notice.setText("one to /from session is null");
					System.out.println("false");
					return;
				}
				
				period_start = (LocalTime) timeListCombo.get(i).getValue();
				period_end = (LocalTime) timeListCombo.get(i+1).getValue();
				
				
				try {
					doctor.createTimeSheet(period_start, period_end, doctorId, dateTime);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
			}			
		});
		return tab1;
	}
	
	public ArrayList<ComboBox> createComboBoxList(){
		ArrayList<LocalTime> localTimeList = setTimeList();
		ObservableList<LocalTime> timeMorningString = FXCollections.observableArrayList(localTimeList.subList(0, 8));
		ObservableList<LocalTime> timeAfternoonString = FXCollections.observableArrayList(localTimeList.subList(7, 18));
		ObservableList<LocalTime> timeEveningString = FXCollections.observableArrayList(localTimeList.subList(17, 24));
		
		ArrayList<ComboBox> timeListCombo = new ArrayList<ComboBox>();
		
		final ComboBox<LocalTime> timeListMorning1 = new ComboBox<LocalTime>(timeMorningString);
		final ComboBox<LocalTime> timeListMorning2 = new ComboBox<LocalTime>(timeMorningString);
		final ComboBox<LocalTime> timeListAfternoon1 = new ComboBox<LocalTime>(timeAfternoonString);
		final ComboBox<LocalTime> timeListAfternoon2 = new ComboBox<LocalTime>(timeAfternoonString);
		final ComboBox<LocalTime> timeListEvening1 = new ComboBox<LocalTime>(timeEveningString);
		final ComboBox<LocalTime> timeListEvening2 = new ComboBox<LocalTime>(timeEveningString);
		
		timeListCombo.add(timeListMorning1);
		timeListCombo.add(timeListMorning2);
		timeListCombo.add(timeListAfternoon1);
		timeListCombo.add(timeListAfternoon2);
		timeListCombo.add(timeListEvening1);
		timeListCombo.add(timeListEvening2);
		
		Render.setPosition(timeListMorning1, 380, 20);
		Render.setPosition(timeListMorning2, 480, 20);
		Render.setPosition(timeListAfternoon1, 380, 60);
		Render.setPosition(timeListAfternoon2, 480, 60);
		Render.setPosition(timeListEvening1, 380, 100);
		Render.setPosition(timeListEvening2, 480, 100);
		
		return timeListCombo;
	}
	
	public void setGroupPosition(Node[] nodeList){
		Double layoutY = 20.0;
		for(int i = 0; i<nodeList.length; i++){
			Render.setPosition(nodeList[i], 40, layoutY);
			layoutY += 40;
		}
	}
	
	public ArrayList<LocalTime> setTimeList(){
		ArrayList<LocalTime> localTimeList = new ArrayList<LocalTime>();
		LocalTime startTime = LocalTime.of(8, 30);
		LocalTime endTime = LocalTime.of(20, 0);
		while(startTime.isBefore(endTime)){
			localTimeList.add(startTime);
			startTime = startTime.plusMinutes(30);
		}
		localTimeList.add(endTime);
		return localTimeList;
	}
}
