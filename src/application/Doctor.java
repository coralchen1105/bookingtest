package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class Doctor {
	
	private int doctorId;
	private String firstName;
	private String lastName;
	
	static final String DB_URL = "jdbc:mysql://127.0.0.1:3306/";
	static final String USER = "root";
	static final String PASS = "";
	
	public Doctor(){
		
	}
	
	public Doctor(int doctorId, String firstName,
			String lastName) {
		//super();
		this.doctorId = doctorId;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Connection connectDB() throws ClassNotFoundException, SQLException{
		  Class.forName("com.mysql.jdbc.Driver");
	      Connection connect = DriverManager.getConnection(DB_URL, USER, PASS);
	      return connect;
	}
	
	
	public ArrayList<TimeSheet> getTimeSheet(LocalDate dateTime, String doctorName) throws ClassNotFoundException, SQLException{
		  ArrayList<TimeSheet> timeSheetList = new ArrayList<TimeSheet>();
		  Connection connect = connectDB();
		  
		  Statement tmpStatement = connect.createStatement();
		  String sql = null;
		  if(dateTime != null){
			  sql = "SELECT * FROM Booking.TimeSheet where idDoctor in (SELECT idDoctor from Booking.Doctor where first_name = '" + doctorName + "') AND date ='" + dateTime +"'";		
		  }else{
			  sql = "SELECT * FROM Booking.TimeSheet where idDoctor in (SELECT idDoctor from Booking.Doctor where first_name = '" + doctorName + "')" ;		
		  }
		  ResultSet resultSet = tmpStatement.executeQuery(sql);
			  
		  while(resultSet.next()){
			  TimeSheet timeSheet = new TimeSheet(resultSet.getLong(1),resultSet.getTime(2),resultSet.getTime(3),resultSet.getDate(6),resultSet.getInt(5),resultSet.getString(4));
			  timeSheetList.add(timeSheet);
		  }
		  
		  if(tmpStatement!=null)
			  tmpStatement.close();
		 
		  if(connect!=null){
			  connect.close();
		  }
	            
		  return timeSheetList;
	  }
	  
	  public ArrayList<Doctor> getDoctor() throws ClassNotFoundException, SQLException{
		  ArrayList<Doctor> doctorList = new ArrayList<Doctor>();
		  Connection connect = connectDB();
		  
		  Statement tmpStatement = connect.createStatement();
		  tmpStatement = connect.createStatement();
		  String sql = "SELECT * FROM Booking.Doctor";
		  ResultSet resultSet = tmpStatement.executeQuery(sql);
		  while(resultSet.next()){
			  Doctor doctor = new Doctor(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3));
			  doctorList.add(doctor);
		  }
		  if(tmpStatement!=null)
			  tmpStatement.close();
		  
		  if(connect!=null){
			  connect.close();
		  }
		  return doctorList;
	  }
	  
	  public void createTimeSheet(LocalTime period_start, LocalTime period_end, int doctorId, LocalDate date) throws ClassNotFoundException, SQLException{
			Connection connect = connectDB();
			this.insertTimeSheet(connect, period_start, period_end, doctorId, date);
			if(connect!=null)
	            connect.close();
		}
	  
	  public void insertTimeSheet(Connection connect, LocalTime period_start, LocalTime period_end, int doctorId, LocalDate date) throws ClassNotFoundException, SQLException{ 
		  LocalTime local_period_start = period_start;
		  LocalTime local_period_end = period_end;
		  LocalTime start_time = null;
		  LocalTime end_time = null;
		 
		  while(local_period_start.isBefore(local_period_end)){
			Statement tmpStatement = connect.createStatement();
			Statement checkStatement = connect.createStatement();
			start_time = local_period_start;
			end_time = start_time.plusMinutes(30);		
			String checkSql = "SELECT * FROM Booking.TimeSheet WHERE start_time = '" + start_time + "'AND end_time = '" + end_time + "' AND idDoctor = '" + doctorId + "'AND date = '" + date + "'"; 
			Boolean isExsited = checkStatement.executeQuery(checkSql).next();	
			if(isExsited){
				local_period_start = end_time; 
				if(checkStatement!=null)
					checkStatement.close();
				System.out.println(start_time + " " + end_time +"exsited!");
				continue;
			}else{
				local_period_start = end_time; 
				String sql = "INSERT INTO Booking.TimeSheet (start_time, end_time, booking_status, idDoctor, date)" + "VALUES ( '" + start_time + "','" + end_time + "', 'No', '" + doctorId + "','" + date + "')" ;
				tmpStatement.executeUpdate(sql);
			}
			
			if(tmpStatement!=null)
				tmpStatement.close();
		  } 	  
	  }
}
