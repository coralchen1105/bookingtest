package application;

public class Patient {
	int patientId;
	String first_name;
	String last_name;
	int age;
	
	public Patient(int patientId,
			String first_name, String last_name,
			int age) {
		super();
		this.patientId = patientId;
		this.first_name = first_name;
		this.last_name = last_name;
		this.age = age;
	}
	
	public void createBooking(int patientId){
		
	}
	
	public void cancelBooking(){
		
	}
	
	public void viewBooking(int patientId){
		
	}
}
