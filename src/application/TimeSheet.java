package application;

import java.math.BigInteger;
import java.sql.Time;
import java.util.Date;

public class TimeSheet {
	private long timeId;
	private Time start_time;
	private Time end_time;
	private Date date;
	private int drId;
	private String booking_status;
	
	
	
	public TimeSheet(long timeId,
			Time start_time, Time end_time,
			Date date, int drId,
			String booking_status) {
		
		this.timeId = timeId;
		this.start_time = start_time;
		this.end_time = end_time;
		this.date = date;
		this.drId = drId;
		this.booking_status = booking_status;
		
	}
	
	
	public Time getStart_time() {
		return start_time;
	}
	public void setStart_time(Time start_time) {
		this.start_time = start_time;
	}
	public long getTimeId() {
		return timeId;
	}
	public void setTimeId(long timeId) {
		this.timeId = timeId;
	}
	public Time getEnd_time() {
		return end_time;
	}
	public void setEnd_time(Time end_time) {
		this.end_time = end_time;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getDrId() {
		return drId;
	}
	public void setDrId(int drId) {
		this.drId = drId;
	}
	public String getBooking_status() {
		return booking_status;
	}
	public void setBooking_status(
			String booking_status) {
		this.booking_status = booking_status;
	}
}
