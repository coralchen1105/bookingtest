package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.math.BigInteger;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Date;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;


public class Main extends Application {
	  
	@Override
	public void start(Stage primaryStage) {
		try { 
			
			AnchorPane root = new AnchorPane();
			Scene scene = new Scene(root,700,700);
			primaryStage.setTitle("Booking");		
			
			TabPane tabPane = new TabPane();
			tabPane.setMinWidth(600.0);
			
			DoctorView doctorView = new DoctorView();
			Tab tab1 = doctorView.init();
			
			PatientView patientView = new PatientView();
			Tab tab2 = patientView.init();
	
			tabPane.getTabs().addAll(tab1,tab2);
			root.getChildren().addAll(tabPane);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
